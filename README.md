# htop

Containerised htop for use with lightweight container operating systems

# Fedora CoreOS

### Install

```
mkdir -p /var/bin

cat > /var/bin/htop <<EOF
docker run -it --rm --pid=host registry.gitlab.com/chrisweeksnz/htop:latest
EOF

chmod 755 /var/bin/htop

```

### Run

```
/var/bin/htop
```

# CoreOS

### Install

```
mkdir -p /opt/bin

cat > /opt/bin/htop <<EOF
docker run -it --rm --pid=host registry.gitlab.com/chrisweeksnz/htop:latest
EOF

chmod 755 /opt/bin/htop

```

### Run

```
/opt/bin/htop
```