FROM alpine:latest

LABEL maintainer="Chris Weeks <chris@weeks.net.nz>"

RUN apk --update add htop

ENTRYPOINT ["htop"]
